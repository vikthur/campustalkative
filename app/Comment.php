<?php

namespace Campustalkative;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	protected $guarded = [
    	'id',
    ];
    public function user(){
    	return $this->belongsTo('Campustalkative\User');
    }
}

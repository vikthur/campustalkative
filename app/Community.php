<?php

namespace Campustalkative;

use Illuminate\Database\Eloquent\Model;

class Community extends Model
{
    protected $guarded = ['id'];

    public function user(){
    	return $this->belongsTo('Campustalkative\User');
    }

    public function preference(){
    	return $this->hasOne('Campustalkative\Preference');
    }

    public function post(){
    	return $this->hasMany('Campustalkative\Post');
    }
}

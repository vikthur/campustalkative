<?php

namespace Campustalkative\Http\Controllers;

use Illuminate\Http\Request;
use Campustalkative\Category;
use Campustalkative\Post;

class AdminController extends Controller
{
    public function index(){
      return view('admin.index');
    }
    public function allPosts(){
      $posts = Post::where('post_type', 'blog')->get();
      return view('admin.posts.index', compact('posts'));
    }
    public function addNewPost(){
      $category = Category::all();
      return view('admin.posts.new_post', compact('category'));
    }
    public function viewUpdatePost($id){
      $post =  Post::findorfail($id);
      return view('admin.posts.view_post', compact('post'));
    }
}

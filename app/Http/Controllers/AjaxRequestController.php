<?php

namespace Campustalkative\Http\Controllers;

use Illuminate\Http\Request;

use Campustalkative\Post;
use Campustalkative\User;
use Campustalkative\School;
use Campustalkative\Country;
use Campustalkative\Comment;

class AjaxRequestController extends Controller
{
    public function uniqueCredentials(Request $request)
    {
    	return $request->all();
    }

    public function getSchools(){
        $schools = School::all();

        return $schools;
    }

    public function getCountries(){
    	return Country::all();
    }

    public function getReplies($id){
        return Comment::where('comment_id', $id)
                        ->with('user')->orderBy('created_at', 'desc')->get();
    }

    public function getUser($id){
        return User::where('id', $id)->with('school','country')->first();
    }

     public function getUserPosts($id){
        return Post::where('user_id', $id)->simplePaginate(10);
    }

    public function getSinglePost($id){
      $post = Post::where('id', $id)->with('user', 'comment')->first();
      return $post;
    }

}

<?php

namespace Campustalkative\Http\Controllers\Auth;

use Auth;
use Illuminate\Support\Facades\Session;
use Campustalkative\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        $username = filter_var(request()->input('email') , FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

        return $username;
    }

    public function login()
    {
        $login = Auth::attempt([
            $this->username() => request()->input('email'),
            'password'=> request()->input('password')], true);

        if($login)
        {
            return redirect($this->redirectTo);
        }else{
            session::flash('auth_error', 'invalid credencials please try again!');
            return redirect()->back();
        }
    }

}

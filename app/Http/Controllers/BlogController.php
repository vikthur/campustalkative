<?php

namespace Campustalkative\Http\Controllers;

use Illuminate\Http\Request;
use Campustalkative\Post;

class BlogController extends Controller
{
    public function index(){
      return view('posts.posts');
    }

    public function getPosts(){
      return Post::with('category', 'user', 'comment')->latest()->simplePaginate(5);
    }
}

<?php

namespace Campustalkative\Http\Controllers;

use Campustalkative\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'comment' => 'required',
            'type' => 'required'
        ]);
        $comment = new Comment();
        $comment->comment = $request->comment;
        $comment->type = $request->type;
        $comment->user_id = ctId();
        if ($request->type == 'comment') {
            $comment->post_id = $request->post_id;
            $comment->save();
            return Comment::where('post_id', $request->post_id)
                            ->with('user')->orderBy('created_at', 'desc')
                            ->get();
        }else{
            $comment->comment_id = $request->comment_id;
            $comment->save();
            return Comment::where('comment_id', $request->comment_id)
                            ->with('user')->orderBy('created_at', 'desc')
                            ->get();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Comment::where('post_id', $id)->with('user')->orderBy('created_at', 'desc')->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $comment = Comment::findorfail($id);

         $smile = [];
         if($comment->smile != null){
            $smile = explode(',', $comment->smile);
         }
         if(in_array(ctId(), $smile)){
            return $comment->smile;
            exit();
            }
         array_push($smile, ctId());
         $smile = implode(',', $smile);
         $comment->update(['smile' => $smile]);
         return $comment->smile;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Comment::findorfail($id);
        $comment->delete();
        return response()->json('ok', 200);
    }
}

<?php

namespace Campustalkative\Http\Controllers;

use Campustalkative\User;
use Illuminate\Http\Request;
use Campustalkative\Community;
use Illuminate\Support\Facades\Storage;

class CommunityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:communities|max:50',
            'description' => 'required|max:100',
        ]);

        $community = new Community();

        $community->name = $request->name;
        $community->description = $request->description;
        $community->user_id = ctId();

        if($file = $request->file('image')){
            $file_mime = explode('/', $file->getClientMimeType());
            if($file_mime[0] != 'image'){
                return response()->json('invalid image file', 403);
                exit();
            }
            $file_name = $file->getClientOriginalName();
            $name = 'community'.ctName().time().$file_name;
            $file->storeAs('public/'.$file_mime[0], $name);
            $community->image = $name;
        }

        $community->save();

        $this->join($community->id);
        $this->makeAdmin($community->id, ctId());

        return response()->json('okk', 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'type' => 'required',
        ]);
        if ($request->type == "join") {
            $this->join($id);
            return response()->json('ok',200);
            exit();
        }

        if($request->type == 'add'){
            $this->add($id, $ids);
            return response()->json('ok',200);
            exit();
        }

        if($request->type == 'admin'){
            $this->makeAdmin($id, $request->user);
            return response()->json('ok', 200);
            exit();
        }
        
        $community = Community::findorfail($id);

        $community->name = $request->name;
        $community->description = $request->description;

        if($file = $request->file('image')){
            $file_mime = explode('/', $file->getClientMimeType());
            if($file_mime[0] != 'image'){
                return response()->json('invalid image file', 403);
                exit();
            }
            Storage::delete($community->image);
            $file_name = $file->getClientOriginalName();
            $name = 'community'.ctName().time().$file_name;
            $file->storeAs('public/'.$file_mime[0], $name);
            $community->image = $name;
        }

        $community->save();

        return response()->json('ok',200);
    }

    public function add($id, $ids){
        $community = Community::findorfail($id);
        $members = [];

        if($community->members != null){
            $members = explode(',', $id);
        }

        foreach ($ids as $new_id) {
            array_push($members, $new_id);
            $this->updateUserColumn($new_id, $community_id);
        }

        $members = implode(',', $members);

        $community->members = $members;

        $community->save();
    }

    public function join($id){
        $community = Community::findorfail($id);
        $members = [];

        if($community->members != null){
        $members = explode(',', $community->members);
        }

        array_push($members, ctId());
        $this->updateUserColumn(ctId(), $id);

        $members = implode(',', $members);

        $community->members = $members;
        $community->save();

    }

    public function updateUserColumn($id, $community_id){

         $user = User::findorfail($id);

         $community = [];

         if($user->community != null){
            $community = explode(',',$user->community);
         }
         array_push($community, $community_id);

         $community = implode(',', $community);

         $user->community = $community;

         $user->save();
    }

    public function makeAdmin($community_id, $user_id){
        $community = Community::findorfail($community_id);
        $admins = [];
        if($community->admins != null){
            $admins = explode(',',$community->admins);
        }

        array_push($admins, $user_id);

        $admins = implode(',', $admins);

        $community->admins = $admins;

        $community->save();
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $community = Community::findorfail($id);
        if($community->members != null){
            $members = explode(',', $community->members);
            foreach ($members as $member) {
                $user = User::findorfail($member);
                $user_communities = explode(',',$user->community);
                $index = array_search($id, $user_communities);
                unset($user_communities[$index]);
                $user->community = implode(',',$user_communities);
                $user->save();
            }
        }
        if($community->image != null){
            Storage::delete('public/image/'.$community->image);
        }
        $community->delete();

        return response()->json('ok!', 200);
    }

}

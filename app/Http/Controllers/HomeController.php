<?php

namespace Campustalkative\Http\Controllers;

use Campustalkative\User;
use Illuminate\Http\Request;
use Campustalkative\Category;
use Campustalkative\Traits\WatchersTrait;

class HomeController extends Controller
{
    use WatchersTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home.home');
    }

    public function getWatchersSuggestion(){
        return $this->WatchersSuggestion();
    }

    public function watching(Request $request){
       $this->validate($request, [
        'watched' => 'required',
       ]);
       $this->watch($request->watched);
       return response()->json('ok', 200);
    }
    public function UserPage($username){
      $user =  User::where('username', $username)->first();
      if($user == null){
        return abort(404);
      }
      return view('home.about', compact('user'));
    }

    public function update(Request $request){
       $user = ctUser();
       $increment = false;
       if($request->school_id != null && $user->school_id == null){
        $increment = true;
       }
       $user->skills = $request->skills;
       $user->about = $request->about;
       $user->school_id = $request->school_id;
       $user->country_id = $request->country_id;
       $user->birthday = $request->dateofbirth;
       $user->twitter_link = $request->twitter_link;
       $user->facebook_link = $request->twitter_link;
       $user->instagram_link = $request->instagram_link;
       $user->profile_photo = $request->profile_picture;
       $user->has_setup = true;
       if($increment){
         $user->school()->increment('count');
         $user->country()->increment('count'); 
       }


       // if ($files = $request->file('photos')) {
       //     $files_array = [];
       //     foreach ($files as $file) {
       //         $file_name = $file->getClientOriginalName();
       //         $file_type = explode('/', $file->getClientMimeType());
       //         if ($file_type[0] != 'image') {
       //             return response()->json('invalid file type!',200);
       //             exit();
       //         }
       //         $name = 'user'.ctName().time().$file_name;
       //         $file->storeAs('public/'.$file_type[0], $name);
       //         array_push($files_array, $name);
       //     }
       //      $user->photos = implode(',', $files_array);
       // }

       $user->save();

       return response()->json('ok',200);

    }
}

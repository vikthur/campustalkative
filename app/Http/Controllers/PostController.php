<?php

namespace Campustalkative\Http\Controllers;

use Auth;
use Campustalkative\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $watching = explode(',',ctUser()->watching);
      array_push($watching, ctId());
      $posts = Post::whereIn('user_id',$watching)
                    ->with('user','comment')->simplePaginate(10);
        //$posts = Post::with('user', 'comment')->get();
        return $posts;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'body' => 'required',
            'category_id' => 'required',
            'post_type' => 'required',
        ]);
        $post = new Post();
        $post->title = $request->title;
        $post->body = $request->body;
        $post->category_id = (int)$request->category_id;

        if($files = $request->file('files')){
            //create an array to store the file names;
            $files_array = [];
            //loop through th files, store each in the postImage folder
            //and push he name into the files array
            foreach ($files as $file) {
                $file_name = $file->getClientOriginalName();
                $file_type = explode('/', $file->getClientMimeType());

             if ($file_type[0] !== 'image' && $file_type[0] !== 'video' && $file_type[0] !== 'audio') {
                    return response()->json('invalid file type!', 500);
                    exit();
                }
                 //concatinate the file name with the user name, and current time to make it unique
                $name = 'posts'.ctName().time().$file_name;
                //save the file in the postImage folder
                $file->storeAs('public/'.$file_type[0], $name);
                //push the name into the files array
                array_push($files_array, $name);

            }
                $files_array = implode(',', $files_array);
                $type = $file_type[0];
                $post->$type = $files_array;

        }
            $post->user_id = ctId();
            $post->post_type = $request->post_type;
            $post->save();
            $post->category()->increment('count');
            if($request->post_type == 'blog'){
              return redirect()->route('all_blog_posts');
            }
            return response()->json('ok', 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findorfail($id);
        return view('posts.singlepost', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::findorfail($id);
        $cat_id = $post->category_id;

        if($request->type == 'smile'){
            $smiles = [];
            if($post->smile != null){
               $smiles = explode(',', $post->smile);
            }
            if(in_array(ctId(), $smiles)){
                return $post->smile;
                exit();
            }
            array_push($smiles, ctId());
            $smiles = implode(',',$smiles);
            $update = $post->update(['smile'=>$smiles]);
            return $post->smile;
        }
        if ($request->type == 'post') {
            if($cat_id != $request->category_id){
            $post->category()->decrement('count');
             }
            // $post = Post::findorfail($id);
            $post->title = $request->title;
            $post->body = $request->body;
            $post->category_id = $request->category_id;

            if ($files = $request->file('files')) {
                $newFiles = [];

                foreach ($files as $file) {
                    $file_name = $file->getClientOriginalName();
                    $file_type = explode('/', $file->getClientMimeType());

                if ($file_type[0] != 'image' && $file_type[0] != 'video' && $file_type[0] != 'audio') {
                    return response()->json('invalid file type!', 500);
                }
                 //concatinate the file name with the user name, and current time to make it unique
                $name = Auth::user()->username.time().$file_name;
                //save the file in the postImage folder
                $store = $file->storeAs('public/'.$file_type[0], $name);
                //push the name into the files array
                array_push($newFiles, $name);
                }
                $newFiles = implode(',', $newFiles);
                $type = $file_type[0];
                $post->$type = $newFiles;
              }
                $post->save();
                if($cat_id != $request->category_id){
                    $post->category()->increment('count');
                }
                if($request->type == 'post'){
                  return redirect()->route('all_blog_posts');
                }
                return response()->json('done!', 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      dd($id);
        $post = Post::findorfail($id);
        if($post->video != null){
            $files = explode(',', $post->video);
            Storage::delete($files);
        }
        if($post->image != null){
            $files = explode(',', $post->image);
            Storage::delete($files);
        }
        if($post->audio != null){
            $files = explode(',', $post->audio);
            Storage::delete($files);
        }
        $post->category()->decrement('count');
        $post->delete();
    }
}

<?php
require __DIR__.'/../../vendor/laravel/framework/src/Illuminate/Support/Facades/Auth.php';


function ctUser(){

	if(Auth::check()){
		return Auth::user();
	}else{
		return null;
	}

}

function ctId(){
	
	return ctUser()->id;
}

function ctName(){
	return ctUser()->username;
}

function ctBirthday($birthday){
	$birthday = explode(',', $birthday);
	return $birthday[1]." ".$birthday[2];
}
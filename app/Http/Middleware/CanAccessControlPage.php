<?php

namespace Campustalkative\Http\Middleware;

use Closure;

class CanAccessControlPage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(ctUser() == null){
            return redirect()->back();
        }
        if(ctUser()->role_id == 1){
            return redirect()->back();
        }

         return $next($request);
        
    }
}

<?php

namespace Campustalkative;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $guarded = [
    	'id',
    ];

    public function comment(){
    	return $this->hasMany('Campustalkative\Comment');
    }
    public function category(){
    	return $this->hasOne('Campustalkative\Category', 'id', 'category_id');
    }
    public function user(){
    	return $this->belongsTo('Campustalkative\User');
    }
}

<?php

namespace Campustalkative;

use Illuminate\Database\Eloquent\Model;

class Preference extends Model
{
    protected $guarded = ['id'];
}

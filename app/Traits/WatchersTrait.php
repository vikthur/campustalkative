<?php

namespace Campustalkative\Traits;

use Auth;
use Campustalkative\User;

trait WatchersTrait
{

    public function watch($id){

	$user = User::findorfail($id);

	$currentUser = ctUser();

        $watchers = [];

	if($user->watchers != null){
	    $watchers = explode(',', $user->watchers);
	}

        array_push($watchers, ctId());

        $watchers = implode(',', $watchers);

        $user->watchers = $watchers;

        $user->save();


        $watching = [];

        if($currentUser->watching != null){
	    $watching = explode(',',$currentUser->watching);
	}

        array_push($watching, $id);

        $watching = implode(',', $watching);

        $currentUser->watching = $watching;

        $currentUser->save();

	}

        public function WatchersSuggestion(){
             // $school_id = ctUser()->schoole_id;
             $watching = explode(',',ctUser()->watching);
             array_push($watching, ctId());
             // $watchers = explode(',',ctUser()->watchers);
             // foreach ($watching as $key => $watch) {
             //   echo $watch.'<br>';
             // }
             //return User::with('school')->get();
             return $users = User::inRandomOrder()->whereNotIn('id',$watching)->limit(10)->get();
        }



}

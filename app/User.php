<?php

namespace Campustalkative;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable //implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $guarded = [
      'id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function preference(){
        return $this->hasOne('Campustalkative\Preference');
    }

    public function community(){
        return $this->hasMany('Campustalkative\Community');
    }

    public function country(){
        return $this->hasOne('Campustalkative\Country', 'id','country_id');
    }
    public function school(){
        return $this->hasOne('Campustalkative\School','id','school_id');
    }
    public function post(){
        return $this->hasMany('Campustalkative\Post');
    }
}

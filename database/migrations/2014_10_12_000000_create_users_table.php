<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fullname');
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->integer('school_id')->nullable();
            $table->blob('cover_photo')->nullable();
            $table->integer('role_id')->default(3);
            $table->string('about')->nullable();
            $table->string('skills')->nullable();
            $table->longtext('watchers')->nullable();
            $table->longtext('watching')->nullable();
            $table->string('community')->nullable();
            $table->boolean('verified')->default(0);
            $table->boolean('has_setup')->default(0);
            $table->string('facebook_link')->nullable();
            $table->string('twitter_link')->nullable();
            $table->string('instagram_link')->nullable();
            $table->blob('profile_photo')->nullable();
            $table->string('birthday')->nullable();
            $table->integer('country_id')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preferences', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->integer('community_id')->nullable();
            $table->boolean('anyone_can_post')->nullable();
            $table->boolean('anyone_can_change_community_name')->nullable();
            $table->boolean('show_contact_details')->nullable();
            $table->boolean('notify_via_email')->nullable();
            $table->boolean('anyone_can_join')->nullable();
            $table->boolean('public_community')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preferences');
    }
}

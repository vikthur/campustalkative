
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');



//global components
Vue.component('app-header' , require('./components/home/Header.vue'));
Vue.component('mobile-view-header' , require('./components/home/MobileViewHeader.vue'));
Vue.component('large-view-header' , require('./components/home/LargeViewComponent.vue'));
Vue.component('account-setup', require('./components/home/SetupComponent.vue'));
Vue.component('ct-posts', require('./components/post/PostComponent.vue'));
Vue.component('personal-details', require('./components/user/PersonalPageComponent.vue'));
Vue.component('user-posts', require('./components/user/UserPostsComponent.vue'));
Vue.component('blog-posts', require('./components/blog/ShowPostsComponent.vue'));
Vue.component('single-post-component', require('./components/blog/SinglePostComponent.vue'));

//vue time ago component
import VueTimeago from 'vue-timeago'
Vue.use(VueTimeago, {
  name: 'Timeago', // Component name, `Timeago` by default
  locale: 'en', // Default locale
  locales: {
    'zh-CN': require('date-fns/locale/zh_cn'),
    'ja': require('date-fns/locale/ja'),
  }
})
//=============================event bus=================================//

//=============================event bus=================================//

//vue toast component
import Toast from 'vue-easy-toast'
Vue.use(Toast)

// vue toaste component
import Toasted from 'vue-toasted';
Vue.use(Toasted)

import SweetModal from 'sweet-modal-vue/src/plugin.js'
Vue.use(SweetModal)

const app = new Vue({
    el: '#app',
});

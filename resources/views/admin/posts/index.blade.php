@extends('layouts.admin')


@section('title')
  Campustalkative - Add New Post
@endsection

@section('content')
<div class="col-md-12">
  @if($posts)
 <ul class="list group">
  @foreach($posts as $post)
   <a href="{{route('viewUpdatePost', $post->id)}}"><li class="list-group-item">{{$post->title}}</li></a>
   @endforeach
 </ul>
 @endif
</div>

@endsection

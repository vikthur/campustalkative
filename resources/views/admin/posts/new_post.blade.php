@extends('layouts.admin')


@section('title')
  Campustalkative - Add New Post
@endsection

@section('content')
<div class="col-md-12">
  <form method="post" action="{{URL::to('post')}}" enctype="multipart/form-data">
    @csrf
  <div class="form-group">
    <label for="posttitle">Post Title</label>
    <input type="text" name="title" class="form-control" id="posttitle" placeholder="post title here...">
  </div>
  <input type="hidden" name="post_type" value="blog">
  <div class="form-group">
    @if($category)
    <label for="category">Choose Post Category</label>
    <select name="category_id" class="custom-select">
      @foreach($category as $cat)
      <option value={{$cat->id}}>{{$cat->name}}</option>
     @endforeach
    </select>
   @endif
  </div>
  <div class="form-group">
    <label for="body">Post Body</label>
    <textarea class="form-control" name="body" id="body" rows="3"></textarea>
  </div>
  <div class="custom-file mb-3">
    <input type="file" name="files[]" class="custom-file-input" id="files" multiple>
    <label class="custom-file-label" for="files">Choose file...</label>
  </div>
  <div class="form-group">
    <button class="btn btn-primary">Submit</button>
  </div>
</form>
</div>

@endsection

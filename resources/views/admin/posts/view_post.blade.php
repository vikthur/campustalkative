@extends('layouts.admin')

@section('title')
 Campustalkative - {{$post->title}}
@endsection

@section('content')

  <div class="col-md-12">
    <form method="post" action="{{URL::to('post', $post->id)}}" enctype="multipart/form-data">
      @csrf
      @method('put')
    <div class="form-group">
      <label for="posttitle">Post Title</label>
      <input type="text" name="title"
             class="form-control"
             id="posttitle"
             value="{{$post->title}}">
    </div>
    <div class="form-group">
        <select name="category_id" class="custom-select">
      <label for="category">Post Category</label>
        <option selected value={{$post->category_id}}>{{$post->category->name}}</option>
      </select>
    </div>
    <div class="form-group">
      <label for="body">Post Body</label>
      <textarea class="form-control" name="body" id="body" rows="3">{{$post->body}}</textarea>
    </div>
    <div class="mb-3 mt-3">
      <?php
        $images = explode(',', $post->image);
        $audios = explode(',', $post->audio);
        $videos = explode(',', $post->video);
      ?>
      @if($post->image != null)
      @foreach($images as $image)
        <img src="/ct/public/storage/image/{{$image}}" width="100px" height="100px" alt="image">
      @endforeach
      @endif
      @if($post->audio != null)
      @foreach($audios as $audio)
        <audio controls>
        <source src="storage/audio/{{$audio}}" type="audio/*">
        </audio>
      @endforeach
      @endif
      @if($post->video != null)
      @foreach($videos as $video)
        <video controls>
        <source src="storage/video/{{$video}}" type="video/*">
        </video>
      @endforeach
      @endif
    </div>
    <input type="hidden" name="type" value="post">
    <div class="custom-file mb-3">
      <input type="file" name="files[]" class="custom-file-input" id="files" multiple>
      <label class="custom-file-label" for="files">Choose file...</label>
    </div>
    <div class="form-group">
      <button class="btn btn-primary">Submit</button>
    </div>
  </form>
  <form method="post" action="{{URL::to('post', $post->id)}}">
    @csrf
    @method('delete')
  <button style="float: right; margin-top: -50px;" class="btn btn-danger">Delete</button>
  </form>
  </div>

@endsection

@extends('layouts.olympus')

@section('title')
    {{ config('app.name') }} - Login
@endsection

@section('content')
<div class="col col-xl-5 col-lg-6 col-md-12 col-sm-12 col-12">           
    <div style="background-color: transparent !important; border: none;" 
                 class="registration-login-form">
        <div class="tab-content">
            <div class="tab-pane" role="tabpanel" data-mh="log-tab">
                <div class="title h6">Login to your CT Account</div>
                    <form class="content" method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="row">
                            <div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">
                                        {{ __('Username or E-mail Address') }}</label>
                                    <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="" name="email" value="{{ old('email') }}" type="text">

                                    @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif

                                </div>
                                <div class="form-group label-floating">
                                    <label class="control-label">
                                        Your Password
                                    </label>
                                    <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="" name="password" type="password">

                                    @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
            
                                    <p>
                                        <a href="{{ route('password.request') }}" class="forgot">Forgot my Password</a>
                                    </p><br><br>

            
                                <div class="form-group label-floating">
                                    <button style="background-color: transparent;" type="submit" class="btn btn-md btn-border c-white full-width">
                                            {{ __('Login!') }}
                                    </button>
                                </div>

                                    @if(Session::has('auth_error'))
                                    <p style="background-color: #ffffff;" class="alert alert-danger">
                                    {{ session('auth_error') }}
                                    </p>
                                    <br><br>
                                    @endif

                                    <p>Don’t you have an account? <a href="{{ route('register') }}">Register Now!</a> it’s really simple and you can start enjoing all the benefits!</p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>      
        </div>
@endsection





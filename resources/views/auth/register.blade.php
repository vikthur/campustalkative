@extends('layouts.olympus')

@section('title')
   Campustalkative - Register
@endsection

@section('content')
<div class="col col-xl-5 col-lg-6 col-md-12 col-sm-12 col-12">           
    <div style="background-color: transparent !important; border: none;" 
                 class="registration-login-form">
        <div class="tab-content">
            <div class="tab-pane active" id="home" role="tabpanel" data-mh="log-tab">
                <div class="title h6">Register to Campustalkative</div>
                <form class="content" method="POST" action="{{ route('register') }}">
                        @csrf
                 <div class="row">
                    <div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
                      <div class="form-group label-floating">
                        <label class="control-label">{{ __('FullName') }}</label>
                             <div class="col-md-12">
                                 <input class="form-control{{ $errors->has('fullname') ? ' is-invalid' : '' }}" name="fullname" value="{{ old('fullname') }}" placeholder="" type="text" required>

                                     @if ($errors->has('fullname'))
                                      <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('fullname') }}</strong>
                                       </span>
                                    @endif
                            </div>
                       </div>

                       <!-- ctName -->
                       <div class="form-group label-floating">
                        <label class="control-label">{{ __('CT Name') }}</label>
                             <div class="col-md-12">
                                 <input id="ctname" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" placeholder="" type="text" required>

                                     @if ($errors->has('username'))
                                      <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('username') }}</strong>
                                       </span>
                                    @endif
                            </div>
                       </div>

                       <!-- email -->
                       <div class="form-group label-floating">
                        <label class="control-label">{{ __('E-mail Address') }}</label>
                             <div class="col-md-12">
                                 <input id="ctmail" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="" type="text" required>

                                     @if ($errors->has('email'))
                                      <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                       </span>
                                    @endif
                            </div>
                       </div>

                        <!-- password -->
                        <div class="form-group label-floating">
                        <label class="control-label">{{ __('Password') }}</label>
                             <div class="col-md-12">
                                 <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="{{ old('password') }}" placeholder="" type="password" required>

                                     @if ($errors->has('password'))
                                      <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                       </span>
                                    @endif
                            </div>
                       </div>

                       <!-- password2 -->
                       <div class="form-group label-floating">
                        <label class="control-label">{{ __('Confirm Password') }}</label>
                             <div class="col-md-12">
                                 <input class="form-control" name="password_confirmation" placeholder="" type="password" required>
                            </div>
                       </div>
                        <p>
                            I accept the <a href="#">Terms and Conditions</a> of the website
                        </p>
                        <br><br>
                        <div class="form-group label-floating">
                        <button style="background-color: transparent;" type="submit" 
                                 class="btn btn-md btn-border c-white full-width">
                            {{ __('Register Now!') }}
                        </button>
                       </div>
                       <p>
                            Already have an account? <a href="{{ route('login') }}">Login </a> Here
                        </p>
                    </div>
                    </div>
                </form>
             </div>
         </div>
     </div>
 </div>
@endsection

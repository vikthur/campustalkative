@extends('layouts.olympus-home-layout')


@section('content')

<div class="container">
  <div class="row">
    <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
      <div class="ui-block">
        <div class="top-header">
          <div class="top-header-thumb">
            <img src="{{ $user->profile_photo }}" alt="nature">
          </div>
          <div class="profile-section">
            <div class="row">
              <div class="col col-lg-5 col-md-5 col-sm-12 col-12">
                <ul class="profile-menu">
                  <li>
                    <a href="#" class="active" disabled>Your Page</a>
                  </li>
                  <li>
                    <a href="home">Home</a>
                  </li>
                  <li>
                    <a href="blog">CT Blog</a>
                  </li>
                </ul>
              </div>
              <div class="col col-lg-5 ml-auto col-md-5 col-sm-12 col-12">
                <ul class="profile-menu">
                  <li>
                    <a href="#" disabled>CT Community</a>
                  </li>
                  <li>
                    <a href="#">CT TV</a>
                  </li>
                  <li>
                    <div class="more">
                      <svg class="olymp-three-dots-icon"><use xlink:href="olympus/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg>
                      <ul class="more-dropdown more-with-triangle">
                        <li>
                          <a href="#">CT Billboard</a>
                        </li>
                        <li>
                          <a href="#">CT Showcase</a>
                        </li>
                      </ul>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <!-- vue buttons -->
                        <app-header></app-header>
            <!-- vue buttons -->
          </div>
          <div class="top-header-author" id="hideforsweetmodal">
            <a href="#" class="author-thumb">
              <img src="{{ $user->profile_photo }}" style="max-width: 100%;" alt="author">
            </a>
            <div class="author-content">
              <a href="#" class="h4 author-name">{{$user->fullname}}</a>
              @if($user->school)
              <div class="country">{{$user->school->school}}</div>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>



<div class="container">
	<div class="row">
		<div class="col col-xl-8 order-xl-2 col-lg-8 order-lg-2 col-md-12 order-md-1 col-sm-12 col-12">
			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Posts by {{$user->username}}</h6>
					<a href="#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>
				</div>
                 <user-posts :id={{$user->id}}></user-posts>
			</div>
		</div>

		<div class="col col-xl-4 order-xl-1 col-lg-4 order-lg-1 col-md-12 order-md-2 col-sm-12 col-12">
			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Personal Info</h6>
					<a href="#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>
				</div>
				<!-- ============= personal details component ==============-->
				<personal-details :id={{$user->id}}></personal-details>
				<!-- ============= personal details component ==============-->
			</div>
		</div>
	</div>
</div>

@endsection

@extends('layouts.olympus-home-layout')

@section('content')

<div class="container">
  <div class="row">
    <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
      <div class="ui-block">
        <div class="top-header">
          <div class="top-header-thumb">
            <img src="{{ ctUser()->profile_photo }}" alt="nature" style="height: 150px;">
          </div>
          <div class="profile-section">
            <div class="row">
              <div class="col col-lg-5 col-md-5 col-sm-12 col-12">
                <ul class="profile-menu">
                  <li>
                    <a href="#" class="active">Home</a>
                  </li>
                  <li>
                    <a href="{{ctName()}}">Home</a>
                  </li>
                  <li>
                    <a href="blog">CT Blog</a>
                  </li>
                </ul>
              </div>
              <div class="col col-lg-5 ml-auto col-md-5 col-sm-12 col-12">
                <ul class="profile-menu">
                  <li>
                    <a href="" disabled>CT Community</a>
                  </li>
                  <li>
                    <a href="" disabled>CT TV</a>
                  </li>
                </ul>
              </div>
            </div>
            <!-- vue buttons -->
                        <app-header></app-header>
            <!-- vue buttons -->
          </div>
          <div class="top-header-author" id="hideforsweetmodal">
            <a href="#" class="author-thumb">
              <img src="{{ ctUser()->profile_photo }}" style="max-width: 100%;" alt="author">
            </a>
            <div class="author-content">
              <a href="#" class="h4 author-name">{{ctUser()->fullname}}</a>
              @if(ctUser()->school)
              <div class="country">{{ctUser()->school->school}}</div>
            @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="container">
  <div class="row">

    <!-- Main Content -->
<ct-posts></ct-posts>
    <div class="col col-xl-6 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-12">
      <div id="newsfeed-items-grid">
        <!-- ===================load posts here================= -->

        <!-- ===================load posts here================= -->
      </div>

      {{-- <a id="load-more-button" href="#" class="btn btn-control btn-more" data-load-link="items-to-load.html" data-container="newsfeed-items-grid">
        <svg class="olymp-three-dots-icon">
          <use xlink:href="olympus/svg-icons/sprites/icons.svg#olymp-three-dots-icon">
          </use>
        </svg>
      </a> --}}
    </div>

    <!-- ... end Main Content -->


    <!-- Left Sidebar -->

    <div class="col col-xl-3 order-xl-1 col-lg-6 order-lg-2 col-md-6 col-sm-12 col-12">

           <!-- ==============profile intro================ -->

           <!-- ================your communities=============== -->

           <!-- ==================campustalkative tv================== -->

    </div>

    <!-- ... end Left Sidebar -->


    <!-- Right Sidebar -->

    <div class="col col-xl-3 order-xl-3 col-lg-6 order-lg-3 col-md-6 col-sm-12 col-12">

          <!-- ================blog posts================== -->

      <!-- =================watchers component============== -->

          <!-- ============featured communities================= -->

    </div>

    <!-- ... end Right Sidebar -->

  </div>
</div>

@endsection

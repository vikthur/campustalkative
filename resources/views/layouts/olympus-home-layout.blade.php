<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Required meta tags always come first -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="x-ua-compatible" content="ie=edge">

	<!-- Main Font -->
    <script src="{{ asset('olympus/js/webfontloader.min.js')}}"></script>
    <script>
        WebFont.load({
            google: {
                families: ['Roboto:300,400,500,700:latin']
            }
        });
    </script>

    <meta name="csrf-token" content="{{ csrf_token() }}">

     <script src="{{ asset('js/app.js') }}" defer></script>

     <script>
     	window.laravel = {
               'userId': '{{ctId()}}',
               'username': '{{ctName()}}',
               'user_photo': '{{ctUser()->profile_photo}}',
               'fullname': '{{ctUser()->fullname}}',
               'csrf_token': '{{ csrf_token() }}'
     	}
     </script>
	<!-- Bootstrap CSS -->
	<link rel="stylesheet"
		  type="text/css"
		  href="{{ asset('olympus/Bootstrap/dist/css/bootstrap-reboot.css') }}">

	<link rel="stylesheet"
	      type="text/css"
	      href="{{ asset('olympus/Bootstrap/dist/css/bootstrap.css')}}">

	<link rel="stylesheet"
	      type="text/css"
	      href="{{ asset('olympus/Bootstrap/dist/css/bootstrap-grid.css')}}">

	<!-- Main Styles CSS -->
	<link rel="stylesheet"
	      type="text/css"
	      href="{{ asset('olympus/css/main.min.css')}}">

	<link rel="stylesheet"
	      type="text/css"
	      href="{{ asset('olympus/css/fonts.min.css')}}">

	      <style>
	      	.input-group-text{
			  	background-color: #182028 !important;
			  	color: #ffffff !important;
			  	font-size: 10px !important;
                }
                textarea#update{
			   	 min-height: 50px !important;
			    }
	      </style>


</head>
<body>

<div id="app">

{{-- <!--- setup component --> --}}
@if(Auth::user()->has_setup == false)
<account-setup></account-setup>
@endif
{{-- <!----setup component --> --}}
  
<!-- Fixed Sidebar Left -->
<div class="fixed-sidebar">
 <!-- ===== fixed sidebar component ========= -->
</div>

<!-- ... end Fixed Sidebar Left -->


<!-- Fixed Sidebar Left -->

<!-- removed -->

<!-- ... end Fixed Sidebar Left -->

<!-- ============ LARGE HEAdER HERE ===============-->
<!-- ============ LARGE HEAdER HERE ============== -->
   <large-view-header></large-view-header>
<!-- ============ LARGE HEAdER HERE ===============-->
<!-- ============ LARGE HEAdER HERE ============== -->


<!-- Responsive Header-BP -->

<!-- ============ MOBILE HEAdER HERE ===============-->
<!-- ============ MOBILE HEAdER HERE ============== -->
   <mobile-view-header></mobile-view-header>
<!-- ============ MOBILE HEAdER HERE ===============-->
<!-- ============ MOBILE HEAdER HERE ============== -->


<!-- ... end Responsive Header-BP -->
<div class="header-spacer"></div>



<!-- Top Header-Profile -->
<!-- move -->
<!-- ... end Top Header-Profile -->

@yield('content')

<a class="back-to-top" href="#">
	<img src="olympus/svg-icons/back-to-top.svg" alt="arrow" class="back-icon">
</a>

</div>


<!-- JS Scripts -->
<script src="{{ asset('olympus/js/jquery-3.2.1.js')}}"></script>

<script defer src="{{ asset('olympus/fonts/fontawesome-all.js')}}"></script>

<script src="{{ asset('olympus/Bootstrap/dist/js/bootstrap.bundle.js')}}"></script>
<script src="{{asset('auth/js/main.js')}}"></script>


</body>

<!-- Mirrored from theme.crumina.net/html-olympus/02-ProfilePage.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 22 Oct 2018 21:40:19 GMT -->
</html>

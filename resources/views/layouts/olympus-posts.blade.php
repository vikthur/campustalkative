<!DOCTYPE html>
<html lang="en">
<head>

<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Required meta tags always come first -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="x-ua-compatible" content="ie=edge">

	<!-- Main Font -->
    <script src="{{ asset('olympus/js/webfontloader.min.js')}}"></script>
    <script>
        WebFont.load({
            google: {
                families: ['Roboto:300,400,500,700:latin']
            }
        });
    </script>

    <meta name="csrf-token" content="{{ csrf_token() }}">

     <script src="{{ asset('js/app.js') }}" defer></script>

     <script>
     	window.laravel = {
               'userId': '{{ctId()}}',
               'username': '{{ctName()}}',
               'user_photo': '{{ctUser()->profile_photo}}',
               'fullname': '{{ctUser()->fullname}}',
               'csrf_token': '{{ csrf_token() }}'
     	}
     </script>
	<!-- Bootstrap CSS -->
	<link rel="stylesheet"
		  type="text/css"
		  href="{{ asset('olympus/Bootstrap/dist/css/bootstrap-reboot.css') }}">

	<link rel="stylesheet"
	      type="text/css"
	      href="{{ asset('olympus/Bootstrap/dist/css/bootstrap.css')}}">

	<link rel="stylesheet"
	      type="text/css"
	      href="{{ asset('olympus/Bootstrap/dist/css/bootstrap-grid.css')}}">

	<!-- Main Styles CSS -->
	<link rel="stylesheet"
	      type="text/css"
	      href="{{ asset('olympus/css/main.min.css')}}">

	<link rel="stylesheet"
	      type="text/css"
	      href="{{ asset('olympus/css/fonts.min.css')}}">



</head>
<body class="body-bg-white">

<div id="app">

<!-- Stunning header -->

<div class="stunning-header bg-primary-opacity">


	<!-- Header Standard Landing  -->

	<div class="header--standard header--standard-landing" id="header--standard">
		<div class="container">
			<div class="header--standard-wrap">

				<a href="#" class="logo">
					<div class="img-wrap">
						<img src="olympus/img/log0o.png" alt="ct">
						<!-- <img src="olympus/img/logo-white.png" alt="ct" class="logo-colored"> -->
					</div>
					<div class="title-block">
						<h6 class="logo-title">Campustalkative</h6>
						<div class="sub-title">connecting campuses and youths</div>
					</div>
				</a>

				<a id="openCustomMenu" href="#" class="open-responsive-menu js-open-responsive-menu">
					<svg class="olymp-menu-icon">
            <use xlink:href="/olympus/svg-icons/sprites/icons.svg#olymp-menu-icon"></use>
          </svg>
				</a>

				<div id="customMenu" class="nav nav-pills nav1 header-menu">
					<div class="mCustomScrollbar">
						<ul>
							<li class="nav-item">
								<a href="#" class="nav-link">Home</a>
							</li>
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle"
								data-hover="dropdown" data-toggle="dropdown"
								href="#" role="button" aria-haspopup="false"
								aria-expanded="false" tabindex='1'>Profile</a>
								<div class="dropdown-menu">
									<a class="dropdown-item" href="home">Home</a>
									<a class="dropdown-item" href="{{ctName()}}">Personal Page</a>
									<a class="dropdown-item" href="#">...</a>
								</div>
							</li>
							<li class="nav-item dropdown dropdown-has-megamenu">
								<a href="#" class="nav-link dropdown-toggle" data-hover="dropdown" data-toggle="dropdown" role="button" aria-haspopup="false" aria-expanded="false" tabindex='1'>Forums</a>
								<div class="dropdown-menu megamenu">
									<div class="row">
										<div class="col col-sm-3">
											<h6 class="column-tittle">Campustalkative</h6>
											<a class="dropdown-item" href="#">Community</a>
											<a class="dropdown-item" href="#">Tv</a>
											<a class="dropdown-item" href="#">Billboard</a>
										</div>
									</div>
								</div>
							</li>
							<li class="nav-item">
								<a href="#" class="nav-link">Terms & Conditions</a>
							</li>
							<li class="nav-item">
								<a href="#" class="nav-link">About</a>
							</li>
							<li class="nav-item">
								<a href="#" class="nav-link">Privacy Policy</a>
							</li>
							<li class="close-responsive-menu js-close-responsive-menu">
								<svg class="olymp-close-icon"><use xlink:href="olympus/svg-icons/sprites/icons.svg#olymp-close-icon"></use></svg>
							</li>
							<li class="nav-item js-expanded-menu">
								<a href="#" class="nav-link">
									<svg class="olymp-menu-icon">
                    <use xlink:href="olympus/svg-icons/sprites/icons.svg#olymp-menu-icon"></use>
                  </svg>
									<svg class="olymp-close-icon">
                    <use xlink:href="olympus/svg-icons/sprites/icons.svg#olymp-close-icon"></use>
                  </svg>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- ... end Header Standard Landing  -->
	<div class="header-spacer--standard"></div>

	<div class="stunning-header-content">
		<h1 class="stunning-header-title">Trending Across Campuses</h1>
		<ul class="breadcrumbs">
			<li class="breadcrumbs-item">
				<a href="#">Home</a>
				<span class="icon breadcrumbs-custom">/</span>
			</li>
			<li class="breadcrumbs-item active">
				<span>Blog posts</span>
			</li>
		</ul>
	</div>

	<div class="content-bg-wrap stunning-header-bg1"></div>
</div>

<!-- End Stunning header -->

@yield('content')

<!-- Footer Full Width -->

<div class="footer footer-full-width" id="footer">
	<div class="container">
		<div class="row">
			<div class="col col-lg-4 col-md-4 col-sm-6 col-6">


				<!-- Widget About -->

				<div class="widget w-about">

					<a href="#" class="logo">
						<div class="img-wrap">
							<img src="olympus/img/logo-white.png" alt="ct">
						</div>
						<div class="title-block">
							<h6 class="logo-title">campustalkative</h6>
							<div class="sub-title">connecting campuses and youth</div>
						</div>
					</a>
					<p>Lorem ipsum dolor sit amet, consect adipisicing elit, sed do eiusmod por incidid ut labore et lorem.</p>
					<ul class="socials">
						<li>
							<a href="#">
								<i class="fab fa-facebook-square" aria-hidden="true"></i>
							</a>
						</li>
						<li>
							<a href="#">
								<i class="fab fa-twitter" aria-hidden="true"></i>
							</a>
						</li>
						<li>
							<a href="#">
								<i class="fab fa-youtube" aria-hidden="true"></i>
							</a>
						</li>
						<li>
							<a href="#">
								<i class="fab fa-google-plus-g" aria-hidden="true"></i>
							</a>
						</li>
						<li>
							<a href="#">
								<i class="fab fa-instagram" aria-hidden="true"></i>
							</a>
						</li>
					</ul>
				</div>

				<!-- ... end Widget About -->

			</div>

			<div class="col col-lg-2 col-md-4 col-sm-6 col-6">


				<!-- Widget List -->

				<div class="widget w-list">
					<h6 class="title">Main Links</h6>
					<ul>
						<li>
							<a href="/login">Landing</a>
						</li>
						<li>
							<a href="/home">Home</a>
						</li>
						<li>
							<a href="/about">About</a>
						</li>
						<!-- <li>
							<a href="#">Events</a>
						</li> -->
					</ul>
				</div>

				<!-- ... end Widget List -->

			</div>
			<div class="col col-lg-2 col-md-4 col-sm-6 col-6">


				<div class="widget w-list">
					<h6 class="title">Your dashboard</h6>
					<ul>
						<li>
							<a href="/home">Home</a>
						</li>
						<li>
							<a href="#">About Page</a>
						</li>
						<li>
							<a href="#">Watchers</a>
						</li>
					</ul>
				</div>

			</div>
			<div class="col col-lg-2 col-md-4 col-sm-6 col-6">


				<div class="widget w-list">
					<h6 class="title">Features</h6>
					<ul>
						<li>
							<a href="#">CT Blog</a>
						</li>
						<li>
							<a href="#">CT Tv</a>
						</li>
						<li>
							<a href="#">CT Community</a>
						</li>
						<li>
							<a href="#">CT Billboard</a>
						</li>
					</ul>
				</div>

			</div>
			<div class="col col-lg-2 col-md-4 col-sm-6 col-6">


				<div class="widget w-list">
					<h6 class="title">Campustalkative</h6>
					<ul>
						<li>
							<a href="#">Privacy</a>
						</li>
						<li>
							<a href="#">Terms & Conditions</a>
						</li>
						<li>
							<a href="#">About Us</a>
						</li>
					</ul>
				</div>

			</div>

			<div class="col col-lg-12 col-md-12 col-sm-12 col-12">


				<!-- SUB Footer -->

				<div class="sub-footer-copyright">
					<span>
						Copyright <a href="#">
              Campustalkative</a> All Rights Reserved 2019
					</span>
				</div>

				<!-- ... end SUB Footer -->

			</div>
		</div>
	</div>
</div>

<!-- ... end Footer Full Width -->




<a class="back-to-top" href="#">
	<img src="olympus/svg-icons/back-to-top.svg" alt="arrow" class="back-icon">
</a>

</div>

<!-- JS Scripts -->
<script src="{{ asset('olympus/js/jquery-3.2.1.js')}}"></script>
<script src="{{ asset('olympus/js/jquery.appear.js')}}"></script>
<script src="{{ asset('olympus/js/jquery.mousewheel.js')}}"></script>
<script src="{{ asset('olympus/js/perfect-scrollbar.js')}}"></script>
<script src="{{ asset('olympus/js/jquery.matchHeight.js')}}"></script>
<script src="{{ asset('olympus/js/svgxuse.js')}}"></script>
<script src="{{ asset('olympus/js/velocity.js')}}"></script>
<script src="{{ asset('olympus/js/ScrollMagic.js')}}"></script>
<script src="{{ asset('olympus/js/popper.min.js')}}"></script>
<script src="{{ asset('olympus/js/smooth-scroll.js')}}"></script>}}
<script src="{{ asset('olympus/js/loader.js')}}"></script>}
<script src="{{ asset('olympus/js/jquery.magnific-popup.js')}}"></script>

<script defer src="{{ asset('olympus/fonts/fontawesome-all.js')}}"></script>

<script src="{{ asset('olympus/Bootstrap/dist/js/bootstrap.bundle.js')}}"></script>
<script src="{{ asset('olympus/Bootstrap/dist/js/bootstrap.bundle.js')}}"></script>

<script>
	$(document).ready(function(){
		$('a#openCustomMenu').click(function(){
			$('div#customMenu').show();
		})
	})
</script>

</body>

</html>

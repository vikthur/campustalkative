<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
	<!-- Required meta tags always come first -->
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>
    	@yield('title')
    </title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
	<meta http-equiv="x-ua-compatible" content="ie=edge">

	<!-- Main Font -->
	<script src="{{ asset('olympus/js/webfontloader.min.js')}}"></script>

	<script>
		WebFont.load({
			google: {
				families: ['Roboto:300,400,500,700:latin']
			}
		});
	</script>

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" 
		  type="text/css" 
		  href="{{ asset('olympus/Bootstrap/dist/css/bootstrap-reboot.css') }}">

	<link rel="stylesheet" 
	      type="text/css" 
	      href="{{ asset('olympus/Bootstrap/dist/css/bootstrap.css')}}">

	<link rel="stylesheet" 
	      type="text/css" 
	      href="{{ asset('olympus/Bootstrap/dist/css/bootstrap-grid.css')}}">

	<!-- Main Styles CSS -->
	<link rel="stylesheet" 
	      type="text/css" 
	      href="{{ asset('olympus/css/main.min.css')}}">

	<link rel="stylesheet" 
	      type="text/css" 
	      href="{{ asset('olympus/css/fonts.min.css')}}">
      <style>
      	.form-control, input, .title{
      		color: #ffffff;
      	}
      	input.form-control{
      	    padding: 8px !important;
      	    border-radius: 10px !important;
      	}
      	.tab-content>.tab-pane{
      		display: block;
      	}
      	body{
      		color: #ffffff;
      	}
      	a{
      		color: #000000;
      	}
      </style>


</head>

<body class="landing-page">

<div class="content-bg-wrap"></div>


<!-- Header Standard Landing  -->

<div class="header--standard header--standard-landing" id="header--standard">
	<div class="container">
		<div class="header--standard-wrap">

			<a href="#" class="logo">
				<div class="img-wrap">
					<img style="width: 100%; heigth: auto; display: block;" src="{{ asset('olympus/img/log.png')}}" alt="Campustalkative">
				</div>
				<div class="title-block">
					<h6 class="logo-title">campustalkative</h6>
					<div class="sub-title">connecting campuses & youths</div>
				</div>
			</a>
		</div>
	</div>
</div>

<!-- ... end Header Standard Landing  -->
<div class="header-spacer--standard"></div>

<div class="container">
	<div class="row display-flex">
		<div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
			<div class="landing-content">
				<h1>Welcome to the Biggest Campus Social Hub in the World</h1>
				<p>We are the best and biggest Campus Social Hub with thousands of active users all around the world. Share you
					gists, give u watchers, showcase yourself, earn fans!
				</p>
				<!-- <a href="#" class="btn btn-md btn-border c-white">Become a Part of it</a> -->
			</div>
		</div>

		@yield('content')

	</div>
</div>


<!-- JS Scripts -->
<script src="{{ asset('olympus/js/jquery-3.2.1.js')}}"></script>
<script src="{{ asset('olympus/js/jquery.appear.js')}}"></script>

<script src="{{ asset('olympusjs/base-init.js')}}"></script>
<script defer src="{{ asset('olympus/fonts/fontawesome-all.js')}}"></script>

<script src="{{ asset('olympus/Bootstrap/dist/js/bootstrap.bundle.js')}}"></script>
<script src="{{asset('auth/js/main.js')}}"></script>

</body>

</html>
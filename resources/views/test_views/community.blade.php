@extends('layouts.app')



@section('content')

<div class="container">
	<div class="row">
		<form action="{{route('comunity.store')}}" method="post" enctype="multipart/form-data">
			@csrf
			<input type="text" name="name" class="form-control">
			<textarea name="description" class="form-control"></textarea>
			<input type="file" name="image">
			<button name="submit" type="submit" class="btn btn-primary">Submit</button>
		</form>
	</div>
	<br><br>
	<hr>
	<div class="row">
		<ul class="list-group">
		@foreach($communities as $community)
		<li class="list-group-item">{{$community->name}}</li>
		<br>
		<form action="{{ route('comunity.update', $community->id) }}" method="post">
			@csrf
			@method('put')
			<input type="hidden" name="type" value="join">
			<button class="btn btn-sm btn-info" type="submit">Join</button>
		</form>
		<br>
		<form action="{{ route('comunity.destroy', $community->id) }}" method="post">
			@csrf
			@method('delete')
			<button class="btn btn-sm btn-danger" type="submit">delete</button>
		</form>
	    </br>
		@endforeach
		</ul>
	</div>
</div>



@endsection
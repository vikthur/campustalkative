@extends('layouts.app')


@section('content')

@if($posts)

  <ul>
  	@foreach($posts as $post)
  	<li class="list-item">
  		@if($post->image != null)
  		<?php  
  		   $images = explode(',', $post->image);
  		?>
  		   @foreach($images as $image)
  		    <img src="storage/image/{{$image}}" width="200" height="100" alt="image">
  		   @endforeach
  		@endif
  		@if($post->video != null)
  		   @foreach($post->video as $video)
  		    <video width="200px" height="100px" controls>
  		    	<source src="storage/video/{{$video}}" type="video/*">
  		    </video> 
  		   @endforeach
  		@endif
  		@if($post->audio != null)
  		   @foreach($post->audio as $audio)
  		    <img src="storage/audio/{{$audio}}" width="200" height="100">
  		   @endforeach
  		@endif
  		<div class="panel-title">
  			{{$post->title}}
  		</div>
  		<div class="panel-body">
  			{{$post->body}}
  		</div>
  		<form action="{{route('post.update', $post->id)}}" method="post">
  			@csrf
  			@method('put')
  			<input type="hidden" name="type" value="smile">
  			<button class="btn btn-sm btn-info">Smile</button>
  		</form>
  		<hr>
  		@foreach($post->comments as $comment)
  		   <span class="alert">{{$comment->comment}}</span>
  		@endforeach
  		<hr>
  		<form action="{{route('comment.store')}}" method="post">
  			@csrf
  			<textarea class="form-control" name="comment" cols="30" rows="1"></textarea>
  			<input type="hidden" name="post_id" value="{{$post->id}}">
  			<input type="hidden" name="type" value="comment">
  			<br>
  			<button class="btn btn-sm btn-primary">Comment</button>
  		</form>
  		<br><br>
  	</li>
  	@endforeach
  </ul>

@endif

@endsection
@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<form action="{{route('updateUser')}}" method="post" 
		enctype="multipart/form-data">
			@csrf
			<input class="form-control" type="text" name="skills">
		    <input type="file" name="photos[]" multiple>
		    <textarea class="form-control" cols="30" rows="1" name="about"></textarea>
		    <select name="school_id" class="form-control">
		    	<option>--Choose School--</option>
		    	@foreach($schools as $school)
		    	<option class="form-control" value="{{$school->id}}">{{$school->school}}</option>
		    	@endforeach
		    </select>
		    <button class="btn btn-primary" type="submit">Update!</button>
		</form>
	</div>
</div>







@endsection
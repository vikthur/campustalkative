<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

//protect all routes from guests!
Route::group(['middleware' => 'auth'], function(){


	
Route::get('/home', 'HomeController@index')->name('home');

Route::post('update_profile', 'HomeController@update');

Route::resource('/post', 'PostController');

Route::resource('/comment', 'CommentController');

Route::resource('/community', 'CommunityController');

Route::post('/ajaxReq', 'AjaxRequestController@uniqueCredentials');

Route::get('/schools', 'AjaxRequestController@getSchools');

Route::get('/countries', 'AjaxRequestController@getCountries');

//get replies
Route::get('/comments/replies/{id}', 'AjaxRequestController@getReplies');

//get watchers suggessions
Route::get('/getWatchersSuggestion', 'HomeController@getWatchersSuggestion');

//watch route
Route::post('/watch', 'HomeController@watching');

//get user -vue
Route::get('/getUser/{id}', 'AjaxRequestController@getUser');

Route::get('/getUserPosts/{id}', 'AjaxRequestController@getUserPosts');

//blog index
Route::get('/blog', 'BlogController@index');

//getblogposts
Route::get('get_blog_posts', 'BlogController@getPosts');

//get single  post
Route::get('getSinglePost/{id}', 'AjaxRequestController@getSinglePost');

});


Route::group(['middleware'=>'auth', 'middleware'=>'control'], function(){

// admin dashboard
Route::get('control/admin', 'AdminController@index');

//show all posts
Route::get('control/posts', 'AdminController@allPosts')->name('all_blog_posts');

//addNew post get
Route::get('control/add_new_post', 'AdminController@addNewPost');

//view_edit single post
Route::get('control/post/{id}', 'AdminController@viewUpdatePost')->name('viewUpdatePost');

Route::get('/test', 'HomeController@getWatchersSuggestion');
});

//user page route
Route::get('/{username}', 'HomeController@UserPage')->middleware('auth');
